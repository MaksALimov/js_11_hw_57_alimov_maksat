import React, {useState} from 'react';
import {nanoid} from "nanoid";
import Calculator from "../../components/Calculator/Calculator";
import SplitCalculatorControls from "../../components/Calculator/CalcControls/SplitCalculatorControls/SplitCalculatorControls";
import IndividualCalcControls from "../../components/Calculator/CalcControls/IndividualCalcControls/IndividualCalcControls";

const CalculatorBuilder = () => {
    // Individual mode states
    const [mode, setMode] = useState('individual');

    const [individualPeople, setIndividualPeople] = useState([]);

    const [individualPrice, setIndividualPrice] = useState([]);

    const [individualShowComponent, setIndividualShowComponent] = useState(false)

    const [individualPercent, setIndividualPercent] = useState(0);

    const [individualDelivery, setIndividualDelivery] = useState(0);

    //Split mode states
    const [splitPeople, setSplitPeople] = useState(0);

    const [splitOrderPrice, setSplitOrderPrice] = useState(0);

    const [splitPercent, setSplitPercent] = useState(0);

    const [splitDelivery, setSplitDelivery] = useState(0);

    const [splitShowComponent, setSplitShowComponent] = useState(false);

    //Individual mode functions
    const IndividualAddPerson = () => {
        setIndividualPeople(people => [...people, {name: '', price: '', id: nanoid()}])
    };

    const onRadioChange = e => {
        setMode(e.target.value)
    };

    const changePersonField = (id, name, value) => {
        setIndividualPeople(people => {
            return people.map(person => {
                if (person.id === id) {
                    return {...person, [name]: value}
                }

                return person;
            });
        })
    };

    const individualCalculate = () => {
        setIndividualPrice(individualPeople.map(price => Math.ceil(price.price)).reduce((acc, price) => price + acc));
        setIndividualShowComponent(true);
    };

    const individualRemovePerson = (id, price) => {
        setIndividualPeople(individualPeople.filter(person => person.id !== id));
        setIndividualPrice(prev => prev - price)
    };

    const getPercent = percent => {
        setIndividualPercent(percent)
    };

    const getDelivery = delivery => {
        setIndividualDelivery(delivery);
    };

    //Split mode functions
    const changeSplitPeopleCount = peopleCount => {
        setSplitPeople(peopleCount);
    }

    const changeSplitOrderPrice = orderPrice => {
        setSplitOrderPrice(orderPrice)
    };

    const getSplitPercent = percent => {
        setSplitPercent(percent);
    };

    const getSplitDelivery = deliverySum => {
        setSplitDelivery(deliverySum);
    };

    const getSplitSum = () => {
        setSplitOrderPrice(prevPrice => prevPrice / 100 * splitPercent + Math.ceil(prevPrice));
        setSplitShowComponent(true);
    };

    return (
        <div>
            <Calculator mode={mode} onRadioChange={onRadioChange}/>
            {mode === 'split' ?
                <SplitCalculatorControls
                    splitPeople={splitPeople}
                    changeSplitPeopleCount={changeSplitPeopleCount}
                    splitOrderPrice={splitOrderPrice}
                    changeSplitOrderPrice={changeSplitOrderPrice}
                    splitPercent={splitPercent}
                    getSplitPercent={getSplitPercent}
                    splitDelivery={splitDelivery}
                    getSplitDelivery={getSplitDelivery}
                    getSplitSum={getSplitSum}
                    splitShowComponent={splitShowComponent}
                /> :
                <IndividualCalcControls
                    individualPeople={individualPeople}
                    changePersonField={changePersonField}
                    individualRemovePerson={individualRemovePerson}
                    IndividualAddPerson={IndividualAddPerson}
                    individualCalculate={individualCalculate}
                    individualPercent={individualPercent}
                    getPercent={getPercent}
                    individualDelivery={individualDelivery}
                    getDelivery={getDelivery}
                    individualShowComponent={individualShowComponent}
                    individualPrice={individualPrice}
                />
            }
        </div>
    )
};

export default CalculatorBuilder;