import React from 'react';
import CalculatorBuilder from "./containers/CalculatorBuilder/CalculatorBuilder";

const App = () => (
    <CalculatorBuilder/>
);

export default App;