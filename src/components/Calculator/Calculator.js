import React from 'react';

const Calculator = ({mode, onRadioChange}) => {
    return (
        <div className="Wrapper">
            <p>
                <label>
                    <input
                        type="radio"
                        checked={mode === 'split'}
                        name="mode"
                        value="split"
                        onChange={onRadioChange}
                    />{' '}
                    Поровну между участниками
                </label>
            </p>
            <p>
                <label>
                    <input
                        type="radio"
                        checked={mode === 'individual'}
                        value="individual"
                        name="mode"
                        onChange={onRadioChange}
                    />{' '}
                    Каждому индивидуально
                </label>
            </p>
        </div>
    );
};

export default Calculator;