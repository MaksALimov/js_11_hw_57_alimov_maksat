import React from 'react';
import './SplitCalculatorControls.css'

const SplitCalculatorControls = (props) => {
    return (
        <div>
            <form className="SplitWrapper">
                <p>
                    <label>
                        Человек:
                        <input
                            type="number"
                            value={props.splitPeople}
                            onChange={e => props.changeSplitPeopleCount(e.target.value)}
                        />
                    </label>
                </p>
                <p>
                    <label>
                        Сумма заказа:
                        <input
                            type="number"
                            value={props.splitOrderPrice}
                            onChange={e => props.changeSplitOrderPrice(e.target.value)}
                        />
                    </label>
                </p>
                <p>
                    <label>
                        Процент чаевых:
                        <input
                            type="number"
                            value={props.splitPercent}
                            onChange={e => props.getSplitPercent(e.target.value)}
                        />
                    </label>
                </p>
                <p>
                    <label>
                        Доставка:
                        <input
                            type="number"
                            value={props.splitDelivery}
                            onChange={e => props.getSplitDelivery(e.target.value)}
                        />
                    </label>
                </p>
                <button type="button" onClick={props.getSplitSum}>Рассчитать</button>
                <div>
                    {props.splitShowComponent ? (
                        <div>
                            Общая сумма заказа: {props.splitDelivery > 0 ? props.splitOrderPrice + Math.ceil(props.splitDelivery) : props.splitOrderPrice}
                            <p>Количество человек: {props.splitPeople}</p>
                            <p>
                                Каждый платит по: {
                                Math.ceil(props.splitDelivery > 0 ? (props.splitOrderPrice + Math.ceil(props.splitDelivery)) / props.splitPeople
                                    : props.splitOrderPrice / props.splitPeople)}
                            </p>
                        </div>
                    ) : null}</div>
            </form>
        </div>
    );
};

export default SplitCalculatorControls;