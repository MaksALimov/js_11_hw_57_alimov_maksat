import React from 'react';
import './IndividualCalcControls.css'

const IndividualCalcControls = (props) => {
    return (
        <div>
            <form className="Wrapper">
                {props.individualPeople.map(person => (
                    <div key={person.id}>
                        <input
                            type="text"
                            placeholder="Name"
                            value={person.name}
                            onChange={e => props.changePersonField(person.id, 'name', e.target.value)}
                        />
                        <input
                            type="number"
                            placeholder="Sum"
                            value={person.price}
                            onChange={e => props.changePersonField(person.id, 'price', e.target.value)}
                        />
                        <button type="button"
                                onClick={() => props.individualRemovePerson(person.id, person.price)}>Delete
                        </button>
                    </div>
                ))}
                <p>
                    <label>
                        Процент чаевых: {' '}
                        <input
                            type="text"
                            value={props.individualPercent}
                            onChange={e => props.getPercent(e.target.value)}/>
                    </label>
                </p>
                <p>
                    <label>
                        Доставка: {' '}
                        <input
                            type="text"
                            value={props.individualDelivery}
                            onChange={e => props.getDelivery(e.target.value)}/>
                    </label>
                </p>
                <div>
                    <button type="button" onClick={props.IndividualAddPerson}>Add</button>
                    <button type="button" onClick={props.individualCalculate}>Calculate</button>
                </div>
                <div>
                    {props.individualShowComponent ? 'Общая сумма заказа' : null} {' '}
                    {props.individualPrice > 0 ? Math.ceil(props.individualPrice / 100 * props.individualPercent + props.individualPrice) : ''}
                </div>
                {props.individualPeople.map((person, i) => {
                    return <p key={i}>
                        {props.individualShowComponent ? person.name : null} {' '}
                        {
                            props.individualShowComponent ?
                                props.individualDelivery > 0 ?
                                    Math.ceil(person.price / 100 * props.individualPercent)
                                    + Math.ceil(person.price) + Math.ceil(props.individualDelivery / props.individualPeople.length)
                                    : Math.ceil(person.price / 100 * props.individualPercent) + Math.ceil(person.price) : null
                        }
                    </p>
                })}
            </form>
        </div>
    );
};

export default IndividualCalcControls;